// 1. Дан массив
// ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
// Развернуть этот массив в обратном направлении
console.log ('dz-13_p1 Punkt 01 -------------------')
let arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'], arr2=[];
let len = arr.length, j = 0;

for (let i = len - 1; i >= 0; i--) {
    arr2[j] = arr[i];
    j++;
}
console.log(arr2);

arr2=[];
let i = arr.length - 1, j = 0;
while (i >= 0) {
    arr2[j] = arr[i];
    i--;
    j++;
}
console.log(arr2);

arr2=[], i = arr.length - 1, j = 0;
do {
    arr2[j] = arr[i];
    i--;
    j++;
}
while (i >= 0)
console.log(arr2);

// 2. Дан массив
// [44, 12, 11, 7, 1, 99, 43, 5, 69]
// Развернуть этот массив в обратном направлении
console.log ('dz-13_p1 Punkt 02 -------------------')
let arr = [44, 12, 11, 7, 1, 99, 43, 5, 69]
let arr2=[], len = arr.length - 1, j = 0;

for (let i = len; i >= 0; i--) {
    arr2[j] = arr[i];
    j++;
}
console.log(arr2);

arr2=[], j = 0;
let i = arr.length - 1
while (i >= 0) {
    arr2[j] = arr[i];
    i--;
    j++;
}
console.log(arr2);

arr2=[], j = 0;
i = arr.length - 1
do {
    arr2[j] = arr[i];
    i--;
    j++;
}
while (i >= 0)
console.log(arr2);

// 3. Дана строка
// let str = 'Hi I am ALex'
// развернуть строку в обратном направлении.
console.log ('dz-13_p1 Punkt 03/06 -------------------')
let str = 'Hi I am ALex', str2 = '', len = str.length - 1;

for (let i = len; i >= 0; i--) {
    str2 += str[i];
}
console.log (str2);

str2 = '';
let i = str.length - 1;
while (i >= 0) {
    str2 += str[i];
    i--;
}
console.log (str2);

str2 = '';
i = str.length - 1
do {
    str2 += str[i];
    i--;
}
while (i >= 0)
console.log (str2);

// 4. Дана строка 
// let str = 'Hi I am ALex'
// сделать ее с с маленьких букв
console.log ('dz-13_p1 Punkt 04 -------------------')
let str = 'Hi I am ALex';
let len = str.length, arr = [];

for (let i = 0; i < len; i++) {
    arr[i] = str[i];
}
len = arr.length;
for (let i = 0; i < len; i++) {
    arr[i] = arr[i].toLowerCase();
}
str = '';
for (let i = 0; i < len; i++) {
    str += arr[i];
}
console.log (str);

str = 'Hi I am ALex'
len = str.length, arr = [];
for (let i = 0; i < len; i++) {
    arr[i] = str[i];
}
len = arr.length;
let i = 0;
while (i < len) {
    arr[i] = arr[i].toLowerCase();
    i++;
}
str = '';
for (i = 0; i < len; i++) {
    str += arr[i];
}
console.log (str);

str = 'Hi I am ALex'
len = str.length, arr = [];
for (let i = 0; i < len; i++) {
    arr[i] = str[i];
}
for (let i in arr) {
    arr[i] = arr[i].toLowerCase();
}
str = '';
for (let i = 0; i < len; i++) {
    str += arr[i];
}
console.log (str);

str = 'Hi I am ALex'
len = str.length, arr = [];
for (let i = 0; i < len; i++) {
    arr[i] = str[i];
}
let i = 0;
for (letter of arr) {
    letter = letter.toLowerCase();
    arr[i] = letter;
    i++;
}
str = '';
for (i = 0; i < len; i++) {
    str += arr[i];
}
console.log (str);

// 5. Дана строка 
// let str = 'Hi I am ALex'
// сделать все буквы большие
console.log ('dz-13_p1 Punkt 05 -------------------')
let str = 'Hi I am ALex';
let len = str.length, arr = [];;

for (let i = 0; i < len; i++) {
    arr[i] = str[i];
}
for (let i = 0; i < len; i++) {
    arr[i] = arr[i].toUpperCase();
}
str = '';
for (let i = 0; i < len; i++) {
    str += arr[i];
}
console.log (str);

str = 'Hi I am ALex'
len = str.length, arr = [];
for (let i = 0; i < len; i++) {
    arr[i] = str[i];
}
let i = 0;
while (i < len) {
    arr[i] = arr[i].toUpperCase();
    i++;
}
str = '';
for (let i = 0; i < len; i++) {
    str += arr[i];
}
console.log (str);

str = 'Hi I am ALex'
len = str.length, arr = [];
for (let i = 0; i < len; i++) {
    arr[i] = str[i];
}
i = 0;
do {
    arr[i] = arr[i].toUpperCase();
    i++;
}
while (i < len)
str = '';
for (let i = 0; i < len; i++) {
    str += arr[i];
}
console.log (str);

str = 'Hi I am ALex'
len = str.length, arr = [];
for (let i = 0; i < len; i++) {
    arr[i] = str[i];
}
for (i in arr) {
    arr[i] = arr[i].toUpperCase();
}
str = '';
for (let i = 0; i < len; i++) {
    str += arr[i];
}
console.log (str);

str = 'Hi I am ALex'
len = str.length, arr = [];
for (let i = 0; i < len; i++) {
    arr[i] = str[i];
}
i = 0;
for (letter of arr) {
    letter = letter.toUpperCase();
    arr[i] = letter;
    i++;
}
str = '';
for (i = 0; i < len; i++) {
    str += arr[i];
}
console.log (str);

// 7. Дан массив
// ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
// сделать все буквы с большой
console.log ('dz-13_p1 Punkt 07 -------------------')
let arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
let len = arr.length;

for (let i = 0; i < len; i++) {
    arr[i] = arr[i].toUpperCase();
}
console.log (arr)

arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
len = arr.length;
for (let i in arr) {
    arr[i] = arr[i].toUpperCase();
}
console.log (arr);

arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
len = arr.length;
let i = 0;
while (i < len) {
    arr[i] = arr[i].toUpperCase();
    i++;
}
console.log (arr);

arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
len = arr.length;
let i = 0;
do {
    arr[i] = arr[i].toUpperCase();
    i++;
}
while (i < len);
console.log (arr);

arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
len = arr.length, i = 0;
for (let val of arr) {
    val = val.toUpperCase();
    arr[i] = val;
    i++;
}
console.log(arr)

// 8. Дан массив
// ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
// сделать все буквы с маленькой
console.log ('dz-13_p1 Punkt 08 -------------------')
let arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
let len = arr.length;

for (let i = 0; i < len; i++) {
    arr[i] = arr[i].toLowerCase();
}
console.log (arr)

arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
for (let i in arr) {
    arr[i] = arr[i].toLowerCase();
}
console.log (arr)

arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
len = arr.length;
let i = 0;
while (i < len) {
    arr[i] = arr[i].toLowerCase();
    i++;
}
console.log (arr)

arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
len = arr.length;
let i = 0;
do {
    arr[i] = arr[i].toLowerCase();
    i++;
}
while (i < len);
console.log (arr);

arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
len = arr.length, i = 0;
for (let val of arr) {
    val = val.toLowerCase();
    arr[i] = val;
    i++;
}
console.log(arr)


// 9. Дано число
// let num = 1234678
// развернуть ее в обратном направлении
console.log ('dz-13_p1 Punkt 09 -------------------')
let num = 1234678
let num_str = String(num), num_str2 = ''; len = num_str.length;

for (let i = len - 1; i >= 0; i--) {
    num_str2 += num_str[i];
}
console.log (num_str2);

num_str2 = '';
let i = len - 1
while (i >= 0) {
    num_str2 += num_str[i];
    i--;
}
console.log (num_str2);

num_str2 = '';
i = len - 1;
do {
    num_str2 += num_str[i];
    i--;
}
while (i >= 0)
console.log (num_str2);

num_str2 = '';
for (let i in num_str) {
    num_str2 += num_str[(len - 1) - i]
}
console.log (num_str2);

num_str2 = '';
let j = 0;
for (let i of num_str) {
    num_str2 += num_str[(len - 1) - j]
    j++
}
console.log (num_str2);

// 10. Дан массив 
// [44, 12, 11, 7, 1, 99, 43, 5, 69]
// отсортируй его в порядке убывания
console.log ('dz-13_p1 Punkt 10 -------------------')
let arr = [44, 12, 11, 7, 1, 99, 43, 5, 69]
let arr_len = arr.length;

for (let i = 0; i < arr_len; i++) {
    for (let j = i; j < arr_len; j++) {
        if (arr[j] > arr[i]) {
            num = arr[i];
            arr[i] = arr[j];
            arr[j] = num;
        }
    }
}
console.log(arr);

arr = [44, 12, 11, 7, 1, 99, 43, 5, 69]
for (let i in arr) {
    for (let j in arr) {
        if (arr[j] < arr[i]) {
            num = arr[i];
            arr[i] = arr[j];
            arr[j] = num;
        }
    }
}
console.log(arr);

let arr = [44, 12, 11, 7, 1, 99, 43, 5, 69]
let a = 0
for (let i of arr) {
    let b = 0;
    for (let j of arr) {
        if (j < i) {
            arr[b] = i;
            i = j;
            arr[a] = i;
        }
        b++;
    }
    a++;
}
console.log(arr);

arr = [44, 12, 11, 7, 1, 99, 43, 5, 69]
let i = 0, j = 0;
while (i < arr_len) {
    j = i;
    while (j < arr_len){
        if (arr[j] > arr[i]) {
            num = arr[i];
            arr[i] = arr[j];
            arr[j] = num;
        }
        j++;
    }
    i++;
}
console.log(arr);

arr = [44, 12, 11, 7, 1, 99, 43, 5, 69]
i = 0, j = 0;
do
{
    j = i;
    do {
        if ((arr[i] == undefined) || (arr[j] == undefined)) {
            break
        }
        if (arr[j] > arr[i]) {
            num = arr[i];
            arr[i] = arr[j];
            arr[j] = num;
        }
        j++;
    }
    while (j < arr_len)
    i++;
}
while (i < arr_len)

console.log(arr);


// 1. Данн массив чисел [12, 22, -1245, -1, 0, 4994, 555]
// -Определить минимальное число
// -Определить максимальное число
// -Определить среднее число между этих чисел
console.log ('dz-13_p2 Punkt 01 -------------------')

let arr = [12, 22, -1245, -1, 0, 4994, 555]
let len = arr.length;
let min = arr[0], max = arr[0], average = arr[0];

for (let i = 1; i < len; i++) {
    for (let j = i; j < len; j++) {
        if (arr[j] < min) {
            min = arr[j]
        }
        if (arr[j] > max) {
            max = arr[j]
        }
    }
    average += arr[i];
}
average = average / len;
console.log(`Min:${min}, Max:${max}, Среднее:${average}`);

min = arr[0], max = arr[0], average = 0;
for (let i in arr) {
    for (let j in arr) {
        if (arr[j] < min) {
            min = arr[j]
        }
        if (arr[j] > max) {
            max = arr[j]
        }
    }
    average += arr[i];
}
average = average / len;
console.log(`Min:${min}, Max:${max}, Среднее:${average}`);

min = arr[0], max = arr[0], average = 0;
for (let num of arr) {
        if (num < min) {
            min = num
        }
        if (num > max) {
            max = num
        }
    average += num;
}
average = average / len;
console.log(`Min:${min}, Max:${max}, Среднее:${average}`);

min = arr[0], max = arr[0], average = arr[0];
let i = 1, j = 1;
while (i < len) {
    while (j < len) {
        if (arr[j] < min) {
            min = arr[j]
        }
        if (arr[j] > max) {
            max = arr[j]
        }
        j++;
    }
    average += arr[i];
    i++;
}
average = average / len;
console.log(`Min:${min}, Max:${max}, Среднее:${average}`);

min = arr[0], max = arr[0], average = arr[0], i = 1, j = 1;
do {
    do  {
        if (arr[j] < min) {
            min = arr[j]
        }
        if (arr[j] > max) {
            max = arr[j]
        }
        j++;
    }
    while (j < len)
    average += arr[i];
    i++;
}
while (i < len)
average = average / len;
console.log(`Min:${min}, Max:${max}, Среднее:${average}`);

// 2. Данн массив чисел [12, 22, -1245, -1, 0, 4994, 555]
// -Посчитать длинну массива, нельзя использовать .length
console.log ('dz-13_p2 Punkt 02 -------------------')
let arr = [12, 22, -1245, -1, 0, 4994, 555]
let len = 0;

for (let i = 0; arr[len] != undefined; i++) {
    len++;
}
console.log(`Длина массива:${len}`)

len = 0;
for (let i of arr) {
    len++;
}
console.log(`Длина массива:${len}`)

len = 0;
for (let i in arr) {
    len++;
    if (arr[i] == undefined) {
        break;
    }
}
console.log(`Длина массива:${len}`)


while (arr[len] != undefined) {
    len++; // len = len + 1; ++len
}
console.log(`Длина массива:${len}`)

len = 0;
do {
    len++;
}
while (arr[len] != undefined)
console.log(`Длина массива:${len}`)


// 3. Дан массив чисел [12, 22, -1245, -1, 0, 4994, 555]
// -Сделать положительные числа отрицательными
// -Сделать отрицательные числа положительными
console.log ('dz-13_p2 Punkt 03 -------------------')
let arr = [12, 22, -1245, -1, 0, 4994, 555]
let len = arr.length;

for (let i = 0; i < len; i++) {
    if (arr[i] != 0) {
        arr[i] = arr[i] * -1;
    }
}
console.log(arr);

arr = [12, 22, -1245, -1, 0, 4994, 555]
for (let i in arr) {
    if (arr[i] != 0) {
        arr[i] = arr[i] * -1;
    }
}
console.log(arr);

arr = [12, 22, -1245, -1, 0, 4994, 555]
let j = 0;
for (let i of arr) {
    if (i != 0) {
        arr[j] = i * -1;
    }
    j++;
}
console.log(arr);

arr = [12, 22, -1245, -1, 0, 4994, 555];
let i = 0;
while (i < len) {
    if (arr[i] != 0) {
        arr[i] = arr[i] * -1;
    }
    i++;
}
console.log(arr);

arr = [12, 22, -1245, -1, 0, 4994, 555], i = 0;
do {
    if (arr[i] != 0) {
        arr[i] = arr[i] * -1;
    }
     i++;
}
while (i < len)
console.log(arr);


// 3. Данн массив чисел [12, 2, 7, 21, 22, -1245, -1, 0, 4994, 555]
// -удалить только отрицательные числа

console.log ('dz-13_p2 Punkt 03-03 -------------------')
let arr = [12, 22, -1245, -1, 0, 4994, 555]
let len = arr.length, arr2 = [];

for (let i = 0; i < len; i++ ) {
    if (arr[i] >= 0) {
        arr2.push(arr[i]);
    }
}
console.log(arr2);

arr2 = [];
for (let i in arr) {
    if ((arr[i] >= 0)) {
        arr2.push(arr[i]);
    }
}
console.log(arr2);

arr2 = [];
for (let i of arr) {
    if (i >= 0) {
        arr2.push(i);
    }
}
console.log(arr2);

arr2 = [];
let i = 0;
while (i < len) {
    if (arr[i] >= 0) {
        arr2.push(arr[i]);
    }
    i++;
}
console.log(arr2);

arr2 = [], i = 0;
do {
    if (arr[i] >= 0) {
        arr2.push(arr[i]);
    }
    i++;
}
while (i < len)
console.log(arr2);

// -удалить все чсла которые больше 12

let arr3 = [];
for (let i = 0; i < len; i++ ) {
    if (arr[i] > 12) {
        arr3.push(arr[i]);
    }

}
console.log(arr3);

arr3 = [];
for (let i in arr) {
    if (arr[i] > 12) {
        arr3.push(arr[i]);
    }

}
console.log(arr3);

arr3 = [];
for (let i of arr) {
    if (i > 12) {
        arr3.push(i);
    }
}
console.log(arr3);

arr3 = [];
let i = 0;
while (i < len) {
    if (arr[i] > 12) {
        arr3.push(arr[i]);
    }
    i++;
}
console.log(arr3);

arr3 = [], i = 0;
do {
    if (arr[i] > 12) {
        arr3.push(arr[i]);
    }
    i++;
}
while (i < len);
console.log(arr3);


// 4. Дан массив массивов
// Отсортировать массивы по годам
let arr = [
	[car = "Nissan",
	model = "Leaf",
	color = "green",
	year = 2015,
	engine = 1.6],
	
    [car = "bmw",
	model = "i3",
	color = "black",
	year = 2018,
	engine = 2.0],
	
    [car = "bmw",
	model = "x5",
	color = "orange",
	year = 2016,
	engine = 3.0],
	
    [car = "mersedes",
	model = "e220",
	color = "blue",
	year = 2024,
	engine = 2.0],
	
    [car = "волга",
	model = "2410",
	color = "black",
	year = 1989,
	engine = 2.4],
]
let len = arr.length;
let item;

for (let i = 0; i < len; i++) {
    for (let j = i; j < len; j++) {
        if (arr[j][3] < arr[i][3]) {
            item = arr[i];
            arr[i] = arr[j];
            arr[j] = item;
        }
    }
}
console.log(arr);

let i = 0, j;
while (i < len) {
    j = i;
    while (j < len) {
        if (arr[j][3] < arr[i][3]) {
            item = arr[i];
            arr[i] = arr[j];
            arr[j] = item;
        }
        j++
    }
    i++;
}
console.log(arr);

i = 0;
do {
    j = i;
    do {
        if (arr[j][3] < arr[i][3]) {
            item = arr[i];
            arr[i] = arr[j];
            arr[j] = item;
        }
        j++
    }
    while (j < len)
    i++;
}
while (i < len)
console.log(arr);

let num;
for (let i in arr) {
    for (let j in arr) {
        if (String(arr[i][3]) < String(arr[j][3])) {
            num = arr[i];
            arr[i] = arr[j];
            arr[j] = num;
        }
    }
}
console.log(arr);

let num, a = 0;
for (let i of arr) {
    let b = 0;
    for (let j of arr) {
        if (String(i[3]) < String(j[3])) {
            num = i;
            arr[a] = j;
            arr[b] = num;
            i = j;
        }
        b++;
    }
    a++;
}
console.log(arr);