// 1. Написати функцію, що буде сортувати числа, наприклад від 1 до 20 та вивести сортовані числа на екран
console.log ('dz-14 Punkt 01 -------------------')
let arr = [4, 6, 14, 1, 18, 20, 5, 16, 3, 8, 2]
let i, j, num;

function sortArrNum(arr) {
    let len = arr.length;
    for (i = 0; i < len; i++) {
        for (j = i; j < len; j++) {
            if (arr[j] < arr[i]) {
                num = arr[i];
                arr[i] = arr[j];
                arr[j] = num;
            }

        }
    }
}
sortArrNum(arr);
console.log(arr)

arr = [4, 6, 14, 1, 18, 20, 5, 16, 3, 8, 2]
var sortNum2 = function (arr) {
    let len = arr.length;
    for (i = 0; i < len; i++) {
        for (j = i; j < len; j++) {
            if (arr[j] < arr[i]) {
                num = arr[i];
                arr[i] = arr[j];
                arr[j] = num;
            }

        }
    }
}
sortNum2(arr);
console.log(arr)

arr = [4, 6, 14, 1, 18, 20, 5, 16, 3, 8, 2]
var sortNum3 = (arr) => {
    let len = arr.length;
    for (i = 0; i < len; i++) {
        for (j = i; j < len; j++) {
            if (arr[j] < arr[i]) {
                num = arr[i];
                arr[i] = arr[j];
                arr[j] = num;
            }

        }
    }
}
sortNum3(arr);
console.log(arr)


// 2 Є масив об'єктів salaries [{name:'ivan', salary:300}, {name:'vova', salary:500}, {name:'zigmund', salary:1300}], що містять заробітні плати.
// Напишіть функцію sumSalaries(salaries), яка повертає суму всіх зарплат - використовуючи вбудовані функції масивів . Якщо об'єкт salaries порожній, то результат повинен бути 0. Виведіть на екран результат.
console.log ('dz-14 Punkt 02 -------------------')
let salaries = [{name:'Ivan', salary:300}, {name:'Vova', salary:500}, {name:'Zigmund', salary:1300}]

let n = 0;
function sumSalaries(accumulator, n){
    return accumulator + n.salary;
}

console.log(`Итого з/п: ${salaries.reduce(sumSalaries, n)}`);


// 3 Створити функцію, що в залежності від зарплати групує співробітників 300-600 середній оклад, 
// 1000-2000 - високий:отримати об'єкт {high:[people with salary], middle:[people with salary], low:[ppl with salary].
//     Якщо категорія пуста, має бути пустий масив.
console.log ('dz-14 Punkt 03 -------------------')    

let salariesFull = [
    {name: 'Ivan', salary: 300},
    {name: 'Vova', salary: 500},
    {name: 'Zigmund', salary: 1300},
    {name: 'Vasya', salary: 1800},
    {name: 'Petya', salary: 600},
    {name: 'SomeMan', salary: 200}
];

let kindOfSalary = {high:[], middle:[], low:[]};

function sortSalary(value, index){
    if (salariesFull[index].salary >= 1000) {
        kindOfSalary.high.push(salariesFull[index].name, salariesFull[index].salary);
    } else if ((salariesFull[index].salary >= 300) && (salariesFull[index].salary <= 600)) {
        kindOfSalary.middle.push(salariesFull[index].name, salariesFull[index].salary);
    } else {kindOfSalary.low.push(salariesFull[index].name, salariesFull[index].salary);}
}

salariesFull.filter(sortSalary);
console.log(kindOfSalary);

let i = 0;
let sumOne = salariesFull.reduce(function(accumulator, i) {
    return accumulator + i.salary;
}, i);
console.log(sumOne);

let j = 0;
let sumTwo = (accumulator, j) => accumulator + j.salary;
console.log(salariesFull.reduce(sumTwo, j));