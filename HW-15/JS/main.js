console.log ('Найти площадь прямоугольника ---------------------------------------')

function square(a, b) {
    return a * b;
}
console.log ('Площадь: ' + square(5, 3));

let square = function(a, b) {
    return a * b;
}
console.log ('Площадь: ' + square(5, 3));

let square = (a, b) => (a * b)
console.log ('Площадь: ' + square(5, 3));

console.log ('Теорема Пифагора ---------------------------------------')

function pifagor(a, b) {
     return Math.sqrt(a * a + b * b);
}
console.log('Гипотенуза = ' + pifagor(12, 9));


let pifagor = function(a, b) {
    return Math.sqrt( a * a + b * b);
}
console.log('Гипотенуза = ' + pifagor(12, 9));

let pifagor = (a, b) => Math.sqrt(a * a + b * b)
console.log('Гипотенуза = ' + pifagor(12, 9));

console.log ('Дискриминант ---------------------------------------')

function discriminant(a, b, c) {
    return (b * b) - (4 * a * c);
}
console.log('D = ' + discriminant(7, 5, 3));

let discriminant = function(a, b, c) {
    return (b * b) - (4 * a * c);
}
console.log('D = ' + discriminant(7, 5, 3));

let discriminant  = (a, b, c) => (b * b) - (4 * a * c)
console.log('D = ' + discriminant(7, 5, 3));


console.log ('Создать только четные числа до 100 ---------------------------------------')

function even(num) {
    let arr = [];
    for (let i = 0; i <= num; i++) {
        if (i % 2 == 0) {
            arr.push(i) 
    }   
}
    return arr;  
}
console.log(even(100));

let even = function(num) {
    let arr = [];
    for (let i = 0; i <= num; i++) {
        if (i % 2 == 0) {
            arr.push(i) 
    }   
}
    return arr;
}
console.log(even(100));

let even = (num) => {
    let arr = [];
    for (let i = 0; i <= num; i++) {
        if (i % 2 == 0) {
            arr.push(i) 
    }   
}
    return arr;
}
console.log(even(100));


function even(arr){
    let arr2 = []
    let i = 0
    for (let val of arr) {
        if (val % 2 == 0) {
        arr2[i] = val;
        i++;
    }
}
    return arr2;
}
let arr = [];
for (let i = 0; i <= 100; i++) {
    arr.push(i);
}
console.log(even(arr));


function even(arr){
        let arr2 = [], j = 0;
    for (let i in arr) {
        if (arr[i] % 2 == 0) {
        arr2[j] = arr[i];
        j++;
        }
    }
    return arr2;
}
let arr = [];
for (let i = 0; i <= 100; i++) {
    arr.push(i);
}
console.log(even(arr));

console.log ('Создать только НЕчетные числа до 100 ---------------------------------------')

function odd(num) {
    let arr = [];
    for (let i = 0; i <= num; i++) {
        if (i % 2 !== 0) {
            arr.push(i) 
    }   
}
    return arr;  
}
console.log(odd(100));

let odd = function(num) {
    let arr = [];
    for (let i = 0; i <= num; i++) {
        if (i % 2 !== 0) {
            arr.push(i) 
    }   
}
    return arr;
}
console.log(odd(100));

let odd = (num) => {
    let arr = [];
    for (let i = 0; i <= num; i++) {
        if (i % 2 !== 0) {
            arr.push(i) 
    }   
}
    return arr;
}
console.log(odd(100));


function odd(arr){
    let arr2 = []
    let i = 0
    for (let val of arr) {
        if (val % 2 !== 0) {
        arr2[i] = val;
        i++;
    }
}
    return arr2;
}
let arr = [];
for (let i = 0; i <= 100; i++) {
    arr.push(i);
}
console.log(odd(arr));


function odd(arr){
        let arr2 = [], j = 0;
    for (let i in arr) {
        if (arr[i] % 2 !== 0) {
        arr2[j] = arr[i];
        j++;
        }
    }
    return arr2;
}
let arr = [];
for (let i = 0; i <= 100; i++) {
    arr.push(i);
}
console.log(odd(arr));


console.log ('Создать функцию по нахождению числа в степени ---------------------------------------')

function exp(a, b) {
    return a**b;
}
let num = 2, rate = 3;
console.log(num + ' в степени ' + rate + ' = ' + exp(num, rate));

let exp = function(a, b) {
    return a**b;
}
let num = 2, rate = 3;
console.log(num + ' в степени ' + rate + ' = ' + exp(num, rate));

let exp  = (a, b) => a**b
let num = 2, rate = 3;
console.log(num + ' в степени ' + rate + ' = ' + exp(num, rate));


// Написать функцию сортировки.
// Функция принимает массив случайных чисел и сортирует их по порядку.
// По дефолту функция сортирует в порядке возростания. 
// Но если передать второй параметр то функция будет сортировать
// по убыванию.
// sort(arr)
// sotrt(arr, 'asc')
// sotrt(arr, 'desc')

console.log ('Написать функцию сортировки ---------------------------------------')

let arr = []
for (let i = 0; i < 10; i++) {
  arr.push(Math.floor(Math.random() * 100))
}

function sortArr (arr, direct) {
  let len = arr.length;
  let num = arr[0], j;
  if ((direct !== 'desc') || (direct == !undefined)) { // по порядку во всех случаях, кроме 'desc'
      for (let i = 0; i < len; i++) {
          for (j = i; j < len; j++) {
            if (arr[j] < arr[i]) {
                num = arr[i];
                arr[i] = arr[j];
                arr[j] = num;             
            }
          }
      }
  } else {  // если 'desc'
    for (let i = 0; i < len; i++) {
        for (j = i; j < len; j++) {
          if (arr[j] > arr[i]) {
              num = arr[i];
              arr[i] = arr[j];
              arr[j] = num;             
          }
        }
    }
  }
  return arr;
}
console.log(sortArr(arr));
console.log(sortArr(arr,'asc'));
console.log(sortArr(arr,'desc'));


let sortArr = function (arr, direct) {
    let len = arr.length;
    let num = arr[0], j;
    if ((direct !== 'desc') || (direct == !undefined)) { // по порядку во всех случаях, кроме 'desc'
        for (let i = 0; i < len; i++) {
            for (j = i; j < len; j++) {
              if (arr[j] < arr[i]) {
                  num = arr[i];
                  arr[i] = arr[j];
                  arr[j] = num;             
              }
            }
        }
    } else {  // если 'desc'
      for (let i = 0; i < len; i++) {
          for (j = i; j < len; j++) {
            if (arr[j] > arr[i]) {
                num = arr[i];
                arr[i] = arr[j];
                arr[j] = num;             
            }
          }
      }
    }
    return arr;
  }
console.log(sortArr(arr));
console.log(sortArr(arr,'asc'));
console.log(sortArr(arr,'desc'));
  

let sortArr = (arr, direct) => {
  let len = arr.length;
  let num = arr[0], j;
  if ((direct !== 'desc') || (direct == !undefined)) { // по порядку во всех случаях, кроме 'desc'
      for (let i = 0; i < len; i++) {
          for (j = i; j < len; j++) {
            if (arr[j] < arr[i]) {
                num = arr[i];
                arr[i] = arr[j];
                arr[j] = num;             
            }
          }
      }
  } else {  // если 'desc'
    for (let i = 0; i < len; i++) {
        for (j = i; j < len; j++) {
          if (arr[j] > arr[i]) {
              num = arr[i];
              arr[i] = arr[j];
              arr[j] = num;             
          }
        }
    }
  }
  return arr;
}
console.log(sortArr(arr));
console.log(sortArr(arr,'asc'));
console.log(sortArr(arr,'desc'));


//  Написать функцию поиска в массиве.
//  функция будет принимать два параметра. 
//  Первый массив, второй посковое число. search(arr, find)
// Т.к. не сказано про первый найденный, делаю поиск всех совпадений в массиве
console.log ('Написать функцию поиска в массиве ---------------------------------------')

let arr = []
for (let i = 0; i < 20; i++) {
  arr.push(Math.floor(Math.random() * 20))
}

function search (arr, find) {
    let len = arr.length, message = [], j = 0;
    for (let i = 0; i < len; i++) {
        if (arr[i] == find) {
            message [j] = 'Число найдено! ' + arr[i] + ' с индексом ' + i; // если чисел несколько в массиве, заполняется массив message
            j++;
        }
    }
    if (j == 0) {
        message[0] = 'Число не найдено!';
    }
    return message;
}
console.log(search(arr, 14));

let search = function (arr, find) {
    let len = arr.length, message = [], j = 0;
    for (let i = 0; i < len; i++) {
        if (arr[i] == find) {
            message [j] = 'Число найдено! ' + arr[i] + ' с индексом ' + i; // если чисел несколько в массиве, заполняется массив message
            j++;
        }
    }
    if (j == 0) {
        message[0] = 'Число не найдено!';
    }
    return message;
}
console.log(search(arr, 14));

let search = (arr, find) => {
    let len = arr.length, message = [], j = 0;
    for (let i = 0; i < len; i++) {
        if (arr[i] == find) {
            message [j] = 'Число найдено! ' + arr[i] + ' с индексом ' + i; // если чисел несколько в массиве, заполняется массив message
            j++;
        }
    }
    if (j == 0) {
        message[0] = 'Число не найдено!';
    }
    return message;
}
console.log(search(arr, 14));